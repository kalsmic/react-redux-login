import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';
import './App.css';
import Header from './components/Header';
import Login from './components/Login';
import Register from './components/Register';
import User from './components/User';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Route exact path='/register' component={Register}/>
        <Route exact path='/user' component={User}/>
        <Route exact path='/login' component={Login}/>
        <Route exact path='/' component={Login}/>
      </div>
    );
  }
}

export default App;
